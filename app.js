(function () {
    'use strict';

    function PomodoroTimer(duration, displayId, endFunction) {
        this.duration = duration;
        this.secondsRemaining = duration;
        this.running = false;
        this.timerDisplay = document.querySelector(displayId);
        this.bonusTime = 0;

        this.reset = function (newTime) {
            this.running = false;
            this.duration = (newTime ? newTime : duration);
            this.timerDisplay.children[1].textContent = '';
            this.secondsRemaining = duration;
            this.bonusTime = 0;
        }
        //expire is when the time runs out, and bonus time starts
        this.expire = function () {
            return endFunction();
        }
    }

    PomodoroTimer.prototype.init = function () {
        //formats the time initially so it displays before you start
        this.formatTime(this.secondsRemaining);
    }

    PomodoroTimer.prototype.start = function () {
        if (this.running) {
            return;
        }
        this.running = true;
        var start = Date.now();
        var _this = this;

        (function timer() {
            //if timer gets paused
            if (!_this.running) {
                _this.duration = _this.secondsRemaining;
                return;
            }
            //Bonus time loop
            if (_this.bonusTime) {
                setTimeout(timer, 1000);
                _this.bonusTime++;
            }
            //if there is time remaining, no bonus time
            else if (_this.secondsRemaining > 0) {
                //wait one second, and call this function again
                _this.secondsRemaining = _this.duration - (((Date.now() - start) / 1000) | 0); //|0 is bitwise OR, and just truncated the fp to an int
                setTimeout(timer, 1000);
            }
            //else (bonus === 0, seconds remain === 0)
            else {
                _this.bonusTime = 1;
                setTimeout(timer, 1000);
                _this.expire();
            }
            //format time to timerDisplay to DOM
            _this.formatTime(_this.secondsRemaining);
        }());
    };

    // PomodoroTimer.prototype.pause = function() {
    //   if(!this.running){
    //     return;
    //   }
    //   this.running = false;
    //   //returns elapsed seconds
    //   return this.duration - this.secondsRemaining + this.bonusTime;
    // };

    //Updates the DOM with the formatted time.
    PomodoroTimer.prototype.formatTime = function (time) {
        this.timerDisplay.children[0].textContent = parseTime(time);

        if (this.bonusTime) {
            this.timerDisplay.children[0].textContent = parseTime(this.duration);
            this.timerDisplay.children[1].textContent = ' +' + parseTime(this.bonusTime);
        }
    };

    function spawnNotification(theBody, theTitle) {
        var options = {
            body: theBody
        };
        var n = new Notification(theTitle, options);
    }

    function parseTime(seconds) {
        var minutes = (seconds / 60) | 0;
        var seconds = (seconds % 60) | 0;
        var parsedTime = {
            'minutes': minutes < 10 ? "0" + minutes : minutes,
            'seconds': seconds < 10 ? "0" + seconds : seconds
        };
        return parsedTime['minutes'] + ':' + parsedTime['seconds'];

    };

    //Main function
    window.onload = function () {
        var timers = [];
        var timeData = [];
        var totalTime = 0;
        var cyclesCompleted = 0;
        var workingTime = 60 * 25; //25 min
        var restingTime = 60 * 5; //5 min
        var title = document.querySelector('#title');

        //Functions that run when timer is completed
        var completeWorkingTimer = function () {
            spawnNotification("Work time complete", 'none.png', 'ehhhh');
            //Notify user via notif, sound, title
        };
        var completeRestingTimer = function () {
            spawnNotification("Get back to work", 'none.png', 'ehhhh');
        };


        //Create and init timers
        var workingTimer = new PomodoroTimer(workingTime, '#working-time', completeWorkingTimer);
        var restingTimer = new PomodoroTimer(restingTime, '#resting-time', completeRestingTimer);
        workingTimer.init();
        restingTimer.init();

        //TODO: don't trigger this immediately, it is kind of annoying and people will say no by default
        Notification.requestPermission();

        //Add event listeners for click events
        var button = document.querySelector('#control-button');
        button.addEventListener('click', function () {
            //DISABLE BUTTON FOR ONE SECOND
            // this.disabled = 'true';

            if (workingTimer.running) {
                switchGradient(restingTimer.duration);

                timeData.push({
                    'work': true,
                    'bonus': workingTimer.bonusTime,
                    'duration': workingTimer.duration,
                    'secondsRemaining': workingTimer.secondsRemaining
                });
                workingTimer.reset();
                workingTimer.init();
                restingTimer.start();
                button.textContent = "Start work";
            }
            else {
                switchGradient(workingTimer.duration);

                if (restingTimer.running) {
                    timeData.push({
                        'rest': true,
                        'bonus': restingTimer.bonusTime,
                        'duration': restingTimer.duration,
                        'secondsRemaining': restingTimer.secondsRemaining
                    });
                }

                restingTimer.reset();
                restingTimer.init();
                workingTimer.start();
                button.textContent = "Take a break";
            }

            updateDisplay(timeData);
        });

        var switchGradient = function (time) {
            var background = document.querySelector('.bg-grad');
            background.style.transition = 'opacity ' + time + 's ease-in-out';
            if (background.classList.contains('toggleGradient')) {
                background.classList.remove("toggleGradient");
            }
            else {
                background.classList.add("toggleGradient");
            }

        }

        var updateDisplay = function (data) {
            if (!data.length) {
                return;
            }

            var stats = document.querySelector('#stats');
            var latestData = data[data.length - 1];
            var latestTime = 0;

            if (latestData['bonus']) {
                latestTime = latestData['bonus'] + latestData['duration'];
            }
            else {
                latestTime = latestData['duration'] - latestData['secondsRemaining'];
            }
            var parsedLatestTime = parseTime(latestTime);

            totalTime += latestTime;

            //Add block to progress bar
            if (latestData['work']) {
                stats.innerHTML += "<div class='time-section work-section'> " + parsedLatestTime + " Work</div>";
            }
            else if (latestData['rest']) {
                stats.innerHTML += "<div class='time-section rest-section'> " + parsedLatestTime + " Rest</div>";
            }
            // stats.lastChild.style.width = ((latestTime / totalTime) * 100).toString() + '%'; 
            stats.lastChild.setAttribute('seconds', latestTime);

            //Was attemping to set width to match time. TODO: make this happen without horrible bugs
            // for(var i = 0; i < stats.children.length; i++){
            //     var time = stats.children[i].getAttribute('seconds');
            //     var width = ((time / totalTime) * 100).toString() + '%';
            //     stats.children[i].style.width = width;
            // }
        }


    };
})();
